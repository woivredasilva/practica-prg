# Notas para los usuarios #

## Compilación del programa  ##

Se ejecuta la siguiente instrucción:

~~~~
make compile
~~~~

## Uso del registro de habitaciones ##

Permite ejecutar las instrucciones que se muestran a continuación

1. Mostrar contactos:

~~~~
java -jar piso.jar show
~~~~

2. Mostrar ayuda

~~~~
java -jar piso.jar help
~~~~

3. Crear un nuevo registro

~~~~
java -jar Piso.jar add <habitación> <calidad> <precio>
~~~~

por ejemplo:

~~~~
java -jar Piso.jar add 115 Buena 3000
~~~~

# Notas para los desarrolladores #

## Generación de Javadoc ##

Se ejecuta la siguiente instrucción:

~~~~
make javadoc
~~~~

## Inspección de Javadoc ##
Suponiendo que tiene instalado _Firefox_, se ejecuta:

~~~~
firefox html/index.html
~~~~

## Sobre el fichero _makefile_ ##

Se han utilizado sentencias específicas de Linux, por tanto, sólo se ejecuta en este sistema operativo.

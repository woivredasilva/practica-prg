package interfaces;

import dominio.*;
import java.lang.ArrayIndexOutOfBoundsException;

public class Interfaz{
	private static Piso piso = new Piso();

	private static void mostrarAyuda(){
		System.out.println("Las instrucciones son las siguientes:"); 
		System.out.println("1. Mostrar habitaciones: java -jar piso.jar show");
		System.out.println("2. Mostrar esta ayuda: java -jar piso.jar help");
		System.out.println("3. Añadir contacto: java -jar piso.jar add <habitación> <calidad> <precio> (<calidad> solo puede ser Baja, Media o Alta), por ejemplo,");
		System.out.println("java -jar piso.jar add 115 Baja 50");

	
	}
	private static void mostrarAyudaCalidad(){
		System.out.println("La calidad solo puede ser Baja , Media o Alta");
	}
	public static void ejecutar(String[] args){
		try
		{
			if (args[0].equalsIgnoreCase("add"))
				if (args[2].equalsIgnoreCase("Baja"))
				piso.annadirHabitacion(new Habitacion(args[1], args[2], args[3]));
				else if (args[2].equalsIgnoreCase("Media"))
				piso.annadirHabitacion(new Habitacion(args[1], args[2], args[3]));
				else if (args[2].equalsIgnoreCase("Alta"))
				piso.annadirHabitacion(new Habitacion(args[1], args[2], args[3]));
				else  mostrarAyudaCalidad(); 
			else if (args[0].equalsIgnoreCase("show"))
				piso.mostrarHabitaciones();
			else if (args[0].equalsIgnoreCase("help")) mostrarAyuda();
			else mostrarAyuda();
		}catch(ArrayIndexOutOfBoundsException ex){
			mostrarAyuda();
		}	
	}
}

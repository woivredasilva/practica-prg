package dominio;

public class Habitacion{
	private String numero;
	private String calidad;	
	private String precio;

	public void setNumero(String numero){
		this.numero = numero;
	}
	
	public String getNumero() {
		return numero;
	}
	 
	public void setCalidad(String calidad) {
		this.calidad = calidad;
	}
	
	public String getCalidad() {
		return calidad;
	}

	public void setPrecio(String precio) {
		this.precio = precio;
	}
	
	public String getPrecio() {
		return precio;
	}


	public Habitacion(String numero, String calidad, String precio){
		this.numero = numero;
		this.calidad = calidad;
		this.precio = precio;
	}

	public String toString() {
		return getNumero() + " " + getCalidad() + " " + getPrecio() + "\n";
	}

	public String paraUsuario(){
		return "La habitación número " + getNumero() + " es de calidad " + getCalidad() + " y tiene un precio de " + getPrecio() + "€ la noche.\n";
	}
}


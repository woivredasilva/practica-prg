package dominio;

import java.util.ArrayList;
import java.io.IOException;
import java.io.File;
import java.util.Scanner;
import java.io.FileWriter;

public class Piso{
	private ArrayList<Habitacion> listaHabitaciones = new ArrayList<>();
	private static String nombreFichero = "habitaciones.txt";

	public Piso(){
		cargarDesdeFichero();
	}

	public void annadirHabitacion(Habitacion habitacion){
		listaHabitaciones.add(habitacion);
		volcarAFichero();
	}
	
	public void mostrarHabitaciones()
	{
		for(Habitacion habitacion :listaHabitaciones)	System.out.println(habitacion.paraUsuario());
	}	
	public void volcarAFichero(){
		try {
			FileWriter fw = new FileWriter(nombreFichero);
			fw.write(this.toString());
			fw.close();
		}catch(IOException ex){
			System.err.println("Error al intentar escribir en el fichero");
		}

	}

	private void cargarDesdeFichero(){
		try{
			File fichero = new File(nombreFichero);
			if (fichero.createNewFile()) {
				System.out.println("Acaba de crearse un nuevo fichero"); 
			} else {
				Scanner sc = new Scanner(fichero);
				while(sc.hasNext()){
					listaHabitaciones.add(new Habitacion(sc.next(), sc.next(), sc.next()));
				}
			}
		}catch(IOException ex){
		}
	}
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(Habitacion habitacion : listaHabitaciones) sb.append(habitacion + "\n");
		return sb.toString();
	}
}





